const logger = require("./logger");

function missingError(argumentName) {
  logger.error(
    `Missing \`${argumentName} argument\`. Expected to be called like this: \`/usr/share/fail2sql-node <name> <time> <ip> <failures> <ipfailures> <ipjailfailures>\``
  );
  process.exit(1);
}

function parseArgs() {
  // 2021-05-11 00:14:58,278 fail2ban.action [9018]: DEBUG /home/ed/fail2sql/fail2sql-node dovecot 1620688498.2255478 104.236.227.138 1 11 1
  // /usr/share/fail2sql-node <name> <time> <ip> <failures> <ipfailures> <ipjailfailures>
  const [
    ,
    ,
    name,
    time,
    ip,
    failures,
    ipFailures,
    ipJailFailures,
  ] = process.argv;

  if (!name) missingError("name");
  if (!time) missingError("time");
  if (!ip) missingError("ip");


  return {
    jail: name,
    date: new Date(time * 1000),
    ip,
    failures,
    ipFailures,
    ipJailFailures,
  };
}

module.exports = parseArgs;
