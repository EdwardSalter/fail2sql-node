const chalk = require("chalk");

module.exports = {
  error(msg) {
    console.error(`${chalk.red("ERROR")} ${msg}`);
  },
};
