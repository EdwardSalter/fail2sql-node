const mysql = require("mysql");
const connection = mysql.createConnection({
  host: process.env.DB_HOST,
  user: process.env.DB_USER,
  password: process.env.DB_PASS,
  database: process.env.DB_DATABASE,
});

function insertRow(
  name,
  date,
  ip,
  longitude,
  latitude,
  country,
  geo,
  failures,
  ipFailures,
  ipJailFailures
) {
  connection.query(
    "INSERT INTO fail2ban (name, date, ip, longitude, latitude, country, geo, failures, ipFailures, ipJailFailures) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
    [
      name,
      date,
      ip,
      longitude,
      latitude,
      country,
      geo,
      failures,
      ipFailures,
      ipJailFailures,
    ],
    function (error, results, fields) {
      console.log("Finished inserting row. Success?", error == null);
      if (error) {
        console.error(error);
        throw error;
      }
      connection.end(() => {
        console.log("Finished tidying up");
      });
      // connected!
    }
  );
}

module.exports = insertRow;
