const geolite2 = require("geolite2");
const fs = require("fs");
const { Reader } = require("maxmind");

const buffer = fs.readFileSync(geolite2.paths.city);
const lookup = new Reader(buffer);

module.exports = function getGeoLocation(ip) {
  const geo = lookup.get(ip);

  if (!geo) return {};

  return {
    lat: geo.location && geo.location.latitude,
    long: geo.location && geo.location.longitude,
    city: geo.city && geo.city.names.en,
    country: geo.country && geo.country.names.en,
    countryCode: geo.country && geo.country.iso_code,
  };
};
