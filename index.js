console.log('Running fail2sql-node script');
require("dotenv").config();

const parseArgs = require("./args");
const getGeo = require("./geoip");
const insertRow = require("./sql");

console.log('Parsing arguments');
const args = parseArgs();

console.log('Getting geo information')
const geo = getGeo(args.ip);

const geoString = [geo.city, geo.country].filter(Boolean).join(", ") || null;
console.log('Inserting row');
insertRow(
  args.jail,
  args.date,
  args.ip,
  geo.long,
  geo.lat,
  geo.countryCode,
  geoString,
  args.failures,
  args.ipFailures,
  args.ipJailFailures
);
